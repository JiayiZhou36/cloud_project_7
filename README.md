# Cloud Project 7
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_7/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_7/-/commits/main)

## Purpose of Project
This project involves data processing utilizing a vector database specifically designed to handle data efficiently. It begins by extracting information from a JSONL file containing descriptions of various vegetables. Subsequently, this data is organized and stored in a hash map format within a vector database, leveraging the capabilities of Qdrant, a specialized vector database system. Then, a query is performed to search for vegetables with vitamin C, and the results are visualized with the vegetable names and their descriptions.

## Requirements
* Ingest data into Vector database
* Perform queries and aggregations
* Visualize output

## Database
![dbase1](/uploads/7ed5eb6c5509cf13b702bb77404f5140/dbase1.png)
![dbase](/uploads/172406786caf80dad1874ee451c56453/dbase.png)

## Visualization
This query extracts two vegetables and their decriptions that contains vitamin c.
![Screenshot_2024-03-23_at_10.12.17_PM](/uploads/dbf57349171c889c7febfc1ed2d74cfd/Screenshot_2024-03-23_at_10.12.17_PM.png)