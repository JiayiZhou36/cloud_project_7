//! This program can be used to set up the collection in Qdrant.
//! `cargo run --release --bin setup_collection`
//! The program will output one dot per point inserted and the current number every 100 points.

use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    vectors_config::Config, CreateCollection, Distance, PointId, PointStruct, Value, VectorParams,
    Vectors, VectorsConfig,
};
use reqwest::Client;

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};

#[derive(Debug, serde::Deserialize)]
struct CohereResponse {
    embeddings: Vec<Vec<f32>>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::builder().build()?;
    let cohere_api_key = std::env::var("COHERE_API_KEY").expect("need COHERE_API_KEY set");
    let collection_name: String = "trial3".to_string();
    let qdrant_uri = std::env::var("QDRANT_URI").expect("need QDRANT_URI set");
    let mut config = QdrantClientConfig::from_url(&qdrant_uri);
    config.api_key = std::env::var("QDRANT_API_KEY").ok();
    let qdrant_client = QdrantClient::new(Some(config)).expect("Failed to connect to Qdrant");

    if !qdrant_client.collection_exists(&collection_name).await? {
        qdrant_client
            .create_collection(&CreateCollection {
                collection_name: collection_name.clone(),
                vectors_config: Some(VectorsConfig {
                    config: Some(Config::Params(VectorParams {
                        size: 1024,
                        distance: Distance::Cosine as i32,
                        ..Default::default()
                    })),
                }),
                ..Default::default()
            })
            .await?;
    }
    let file = std::env::args()
        .nth(1)
        .expect("Needs the JSONL file in the first argument");
    let mut points: Vec<PointStruct> = Vec::new();
    let abstracts = File::open(&file).expect("couldn't open JSONL");
    let abstracts = BufReader::new(abstracts);
    let stdout = std::io::stdout();
    let mut stdout = stdout.lock();
    let mut i = 1;
    for line in abstracts.lines() {
        let payload: HashMap<String, Value> = serde_json::from_str(&line?)?;
        let text = payload.get("desc").expect("desc field not found in payload").as_str().expect("text isn't a string");

        // Parse JSON response
        let CohereResponse { embeddings } = client
            .post("https://api.cohere.ai/embed")
            .header("Authorization", &format!("Bearer {}", cohere_api_key))
            .header("Content-Type", "application/json")
            .header("Cohere-Version", "2021-11-08")
            .body(format!("{{\"texts\":[\"{}\"],\"model\":\"small\"}}", text))
            .send()
            .await?
            .json()
            .await?;

        for e in embeddings {
            points.push(PointStruct {
                id: Some(PointId::from(i as u64)),
                payload: payload.clone(),
                vectors: Some(Vectors::from(e)),
            });
            write!(stdout, ".")?;

            write!(stdout, "{}", i)?;
            stdout.flush()?;
            i += 1;
        }
    }
    qdrant_client
        .upsert_points_blocking(&collection_name, None, points, None)
        .await?;
    Ok(())
}
